const { exec } = require("child_process")
const sha256 = require('js-sha256')
const BN = require('bn.js')

const aPrivKey = new BN(1)
const amount = new BN(1)
const bPrivKey = new BN(2)

const run = async (cmd) => {
    return new Promise((resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            if (error) {
                console.log(error, stderr)
                reject(stderr)
                return
            }
            if (stderr) {
                console.log(error, stderr)
                reject(stderr)
                return
            }
            resolve(stdout)
        })
    })
}

const zokrates = async (cmd, dir = ".") => {
    let result
    try {
        result = await run(`cd ${dir} && zokrates ${cmd}`)    
    } catch (error) {
        console.log(error)
    }
    console.log(result)
}


const genWitness = async (name, params) => {
    const dir = `./zk/${name}/`
    await zokrates(`--verbose compute-witness -i ${name}.bin -o ${name}.witness -a ${params.map(a => a.toString()).join(" ")}`, dir)
}

const genProof = async (name) => {
    const dir = `./zk/${name}/`
    await zokrates(`--verbose generate-proof -i ${name}.bin -p ${name}-proving.key -w ${name}.witness -j ${name}-proof.json`, dir)
}

const main = async () => {
    let params = []

    const privKeyArray = aPrivKey.toArray("be", 32)
    
    const amountArray = amount.toArray("be", 32)
    const commitmentData = sha256.arrayBuffer(amountArray.concat(sha256.array(privKeyArray)))

    const view = new DataView(commitmentData)
    params = [aPrivKey.toString(), amount.toString(), view.getUint32(0), view.getUint32(4), view.getUint32(8), view.getUint32(12), view.getUint32(16), view.getUint32(20), view.getUint32(24), view.getUint32(28)]
    

    await genWitness("mint", params)
    await genProof("mint")

    await genWitness("burn", params)
    await genProof("burn")

    const bPrivKeyArray = bPrivKey.toArray("be", 32)
    const bPubKeyArray = sha256.array(bPrivKeyArray)
    const bPubKeyView = new DataView(sha256.arrayBuffer(bPrivKeyArray))

    const bCommitmentData = sha256.arrayBuffer(amountArray.concat(bPubKeyArray))

    const bView = new DataView(bCommitmentData)
    const transferParams = [
        aPrivKey.toString(),
        bPubKeyView.getUint32(0), bPubKeyView.getUint32(4), bPubKeyView.getUint32(8), bPubKeyView.getUint32(12), bPubKeyView.getUint32(16), bPubKeyView.getUint32(20), bPubKeyView.getUint32(24), bPubKeyView.getUint32(28),
        amount.toString(),
        view.getUint32(0), view.getUint32(4), view.getUint32(8), view.getUint32(12), view.getUint32(16), view.getUint32(20), view.getUint32(24), view.getUint32(28),

        bView.getUint32(0), bView.getUint32(4), bView.getUint32(8), bView.getUint32(12), bView.getUint32(16), bView.getUint32(20), bView.getUint32(24), bView.getUint32(28)
    ]

    await genWitness("transfer", transferParams)
    await genProof("transfer")
}


main().then(() => {
    console.log("Done")
})