const { exec } = require("child_process")

const run = async (cmd) => {
    return new Promise((resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            if (error) {
                reject(stderr)
                return;
            }
            if (stderr) {
                reject(stderr)
                return;
            }
            resolve(stdout)
        })
    })
}

const zokrates = async (cmd, dir = ".") => {
    let result
    try {
        result = await run(`cd ${dir} && zokrates ${cmd}`)    
    } catch (error) {
        console.log(error)
    }
    console.log(result)
}


const buildZok = async (name) => {
    const dir = `./zk/${name}/`
    const contractName = name[0].toUpperCase() + name.substring(1)

    await run(`cd ${dir} && find . -type f ! -name '*.zok' -delete`)
    await zokrates(`compile -i ${name}.zok -o ${name}.bin`, dir)
    await zokrates(`setup -i ${name}.bin -p ${name}-proving.key -v ${name}-verifing.key`, dir)
    await zokrates(`export-verifier -i ${name}-verifing.key -o ../../contracts/${contractName}Verifier.sol`, dir)

    if(process.platform == "darwin") {
        await run(`sed -i '' 's/contract\ Verifier\ {/contract\ ${contractName}Verifier\ {/g' ./contracts/${contractName}Verifier.sol`)
    }else {
        await run(`sed -i 's/contract\ Verifier\ {/contract\ ${contractName}Verifier\ {/g' ./contracts/${contractName}Verifier.sol`)
    }
    
}

const main = async () => {
    await buildZok("mint")
    await buildZok("transfer")
    await buildZok("burn")
}


main().then(() => {
    console.log("Done")
})