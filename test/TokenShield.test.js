const sha256 = require('js-sha256')
const truffleAssert = require('truffle-assertions')
const BN = require('bn.js')
const fs = require('fs')

const TokenShield = artifacts.require("TokenShield")
const TestToken = artifacts.require("TestToken")

const hexToBn = (string) => {
    return web3.utils.toBN(string)
}

const readProofFile = (name) => {
    const data = fs.readFileSync(`./zk/${name}/${name}-proof.json`)
    const json = JSON.parse(data)
    return [
        json.proof.a[0],
        json.proof.a[1],

        json.proof.b[0][0],
        json.proof.b[0][1],
        json.proof.b[1][0],
        json.proof.b[1][1],

        json.proof.c[0],
        json.proof.c[1],
    ].map(sValue => {
        return hexToBn(sValue)
    })
}

let tokenShield
let testToken
const mintProof = readProofFile("mint")
const burnProof = readProofFile("burn")
const transferProof = readProofFile("transfer")

const aPrivKey = new BN(1)
const bPrivKey = new BN(2)
const amount = new BN(1)

contract("TokenShield", accounts => {
    beforeEach(async () => {
        tokenShield = await TokenShield.deployed()
        
        const tokenAddress = await tokenShield.token()
        testToken = await TestToken.at(tokenAddress)
    }),

    it("mint", async () => {
        await testToken.approve(tokenShield.address, amount)
        
        const privKeyArray = aPrivKey.toArray("be", 32)
        
        const amountArray = amount.toArray("be", 32)
        const commitmentData = sha256.arrayBuffer(amountArray.concat(sha256.array(privKeyArray)))
        
        const view = new DataView(commitmentData)
        const commitment = [view.getUint32(0), view.getUint32(4), view.getUint32(8), view.getUint32(12), view.getUint32(16), view.getUint32(20), view.getUint32(24), view.getUint32(28)]

        await tokenShield.mint(amount, commitment, mintProof)
    })

    it("burn", async () => {
        await testToken.approve(tokenShield.address, amount)
        
        const privKeyArray = aPrivKey.toArray("be", 32)
        
        const amountArray = amount.toArray("be", 32)
        const commitmentData = sha256.arrayBuffer(amountArray.concat(sha256.array(privKeyArray)))
        
        const view = new DataView(commitmentData)
        const commitment = [view.getUint32(0), view.getUint32(4), view.getUint32(8), view.getUint32(12), view.getUint32(16), view.getUint32(20), view.getUint32(24), view.getUint32(28)]

        await tokenShield.mint(amount, commitment, mintProof)

        await tokenShield.burn(amount, commitment, burnProof)
    })

    it("transfer", async () => {

        await testToken.approve(tokenShield.address, amount)

        const privKeyArray = aPrivKey.toArray("be", 32)
        
        const amountArray = amount.toArray("be", 32)
        const commitmentData = sha256.arrayBuffer(amountArray.concat(sha256.array(privKeyArray)))
        
        const view = new DataView(commitmentData)
        const commitment = [view.getUint32(0), view.getUint32(4), view.getUint32(8), view.getUint32(12), view.getUint32(16), view.getUint32(20), view.getUint32(24), view.getUint32(28)]

        await tokenShield.mint(amount, commitment, mintProof)

        
        const bPrivKeyArray = bPrivKey.toArray("be", 32)
        
        const bCommitmentData = sha256.arrayBuffer(amountArray.concat(sha256.array(bPrivKeyArray)))
        
        const bView = new DataView(bCommitmentData)
        const bCommitment = [bView.getUint32(0), bView.getUint32(4), bView.getUint32(8), bView.getUint32(12), bView.getUint32(16), bView.getUint32(20), bView.getUint32(24), bView.getUint32(28)]

        await tokenShield.transfer(commitment, bCommitment, transferProof)
    })
})

