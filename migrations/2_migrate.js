const TokenShield = artifacts.require("TokenShield")
const TestToken = artifacts.require("TestToken")

const MintVerifier = artifacts.require("MintVerifier")
const TransferVerifier = artifacts.require("TransferVerifier")
const BurnVerifier = artifacts.require("BurnVerifier")

module.exports = async (deployer, network, accounts) => {
    const owner1 = accounts[0]
    const owner2 = accounts[1]

    await deployer.deploy(TestToken, owner1, owner2)

    const testToken = await TestToken.deployed()


    await deployer.deploy(MintVerifier)
    const mintVerifier = await MintVerifier.deployed()

    await deployer.deploy(TransferVerifier)
    const transferVerifier = await TransferVerifier.deployed()

    await deployer.deploy(BurnVerifier)
    const burnVerifier = await BurnVerifier.deployed()

    await deployer.deploy(TokenShield, testToken.address, mintVerifier.address, transferVerifier.address, burnVerifier.address)

    const owner1Balance = await testToken.balanceOf(owner1)
    const owner2Balance = await testToken.balanceOf(owner2)
    console.log("Owner1 balance: ", owner1Balance.toString())
    console.log("Owner2 balance: ", owner2Balance.toString())

}
