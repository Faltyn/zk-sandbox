// This file is MIT Licensed.
//
// Copyright 2017 Christian Reitwiessner
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
pragma solidity ^0.8.0;
library Pairing {
    struct G1Point {
        uint X;
        uint Y;
    }
    // Encoding of field elements is: X[0] * z + X[1]
    struct G2Point {
        uint[2] X;
        uint[2] Y;
    }
    /// @return the generator of G1
    function P1() pure internal returns (G1Point memory) {
        return G1Point(1, 2);
    }
    /// @return the generator of G2
    function P2() pure internal returns (G2Point memory) {
        return G2Point(
            [10857046999023057135944570762232829481370756359578518086990519993285655852781,
             11559732032986387107991004021392285783925812861821192530917403151452391805634],
            [8495653923123431417604973247489272438418190587263600148770280649306958101930,
             4082367875863433681332203403145435568316851327593401208105741076214120093531]
        );
    }
    /// @return the negation of p, i.e. p.addition(p.negate()) should be zero.
    function negate(G1Point memory p) pure internal returns (G1Point memory) {
        // The prime q in the base field F_q for G1
        uint q = 21888242871839275222246405745257275088696311157297823662689037894645226208583;
        if (p.X == 0 && p.Y == 0)
            return G1Point(0, 0);
        return G1Point(p.X, q - (p.Y % q));
    }
    /// @return r the sum of two points of G1
    function addition(G1Point memory p1, G1Point memory p2) internal view returns (G1Point memory r) {
        uint[4] memory input;
        input[0] = p1.X;
        input[1] = p1.Y;
        input[2] = p2.X;
        input[3] = p2.Y;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 6, input, 0xc0, r, 0x60)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require(success);
    }


    /// @return r the product of a point on G1 and a scalar, i.e.
    /// p == p.scalar_mul(1) and p.addition(p) == p.scalar_mul(2) for all points p.
    function scalar_mul(G1Point memory p, uint s) internal view returns (G1Point memory r) {
        uint[3] memory input;
        input[0] = p.X;
        input[1] = p.Y;
        input[2] = s;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 7, input, 0x80, r, 0x60)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require (success);
    }
    /// @return the result of computing the pairing check
    /// e(p1[0], p2[0]) *  .... * e(p1[n], p2[n]) == 1
    /// For example pairing([P1(), P1().negate()], [P2(), P2()]) should
    /// return true.
    function pairing(G1Point[] memory p1, G2Point[] memory p2) internal view returns (bool) {
        require(p1.length == p2.length);
        uint elements = p1.length;
        uint inputSize = elements * 6;
        uint[] memory input = new uint[](inputSize);
        for (uint i = 0; i < elements; i++)
        {
            input[i * 6 + 0] = p1[i].X;
            input[i * 6 + 1] = p1[i].Y;
            input[i * 6 + 2] = p2[i].X[1];
            input[i * 6 + 3] = p2[i].X[0];
            input[i * 6 + 4] = p2[i].Y[1];
            input[i * 6 + 5] = p2[i].Y[0];
        }
        uint[1] memory out;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 8, add(input, 0x20), mul(inputSize, 0x20), out, 0x20)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require(success);
        return out[0] != 0;
    }
    /// Convenience method for a pairing check for two pairs.
    function pairingProd2(G1Point memory a1, G2Point memory a2, G1Point memory b1, G2Point memory b2) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](2);
        G2Point[] memory p2 = new G2Point[](2);
        p1[0] = a1;
        p1[1] = b1;
        p2[0] = a2;
        p2[1] = b2;
        return pairing(p1, p2);
    }
    /// Convenience method for a pairing check for three pairs.
    function pairingProd3(
            G1Point memory a1, G2Point memory a2,
            G1Point memory b1, G2Point memory b2,
            G1Point memory c1, G2Point memory c2
    ) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](3);
        G2Point[] memory p2 = new G2Point[](3);
        p1[0] = a1;
        p1[1] = b1;
        p1[2] = c1;
        p2[0] = a2;
        p2[1] = b2;
        p2[2] = c2;
        return pairing(p1, p2);
    }
    /// Convenience method for a pairing check for four pairs.
    function pairingProd4(
            G1Point memory a1, G2Point memory a2,
            G1Point memory b1, G2Point memory b2,
            G1Point memory c1, G2Point memory c2,
            G1Point memory d1, G2Point memory d2
    ) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](4);
        G2Point[] memory p2 = new G2Point[](4);
        p1[0] = a1;
        p1[1] = b1;
        p1[2] = c1;
        p1[3] = d1;
        p2[0] = a2;
        p2[1] = b2;
        p2[2] = c2;
        p2[3] = d2;
        return pairing(p1, p2);
    }
}

contract BurnVerifier {
    using Pairing for *;
    struct VerifyingKey {
        Pairing.G1Point alpha;
        Pairing.G2Point beta;
        Pairing.G2Point gamma;
        Pairing.G2Point delta;
        Pairing.G1Point[] gamma_abc;
    }
    struct Proof {
        Pairing.G1Point a;
        Pairing.G2Point b;
        Pairing.G1Point c;
    }
    function verifyingKey() pure internal returns (VerifyingKey memory vk) {
        vk.alpha = Pairing.G1Point(uint256(0x16b056fac4cb9560cdbe21733e62ac0f61d36971f44ec02870294865e51a8518), uint256(0x057bd7b01b9bd68267935d7dcbf0c12f1e26bfc52489977d617e93282cc56b76));
        vk.beta = Pairing.G2Point([uint256(0x30346ead3fdcb4ed9c07c9543695abd316e22c6de0a5470705d14f0a3c4c140a), uint256(0x00706690af632ae9ade0273dc453c089a578bab9108f7a7637ca17ecfde7b589)], [uint256(0x0c877c7489bab9c2b0b68dabd2f3012654df87a076225aa713d37d476a5270c3), uint256(0x2414a21eb95ce5f1034456e5eba7f7645df18851d56f6350dc61c051a535b725)]);
        vk.gamma = Pairing.G2Point([uint256(0x1982e006ae41143539b6270344c3e43e452df2a2bad373301f4a58006b0905c2), uint256(0x0381bb18d8b572dc5d29a60ca43fa5f2bbfe3f987bdf1b352a40f8027422e366)], [uint256(0x0adde833d7f21b44ef69e3811a68b57b0ce1e371714c33c43b157fe701307a00), uint256(0x2b8b2d96b5a929c040d2c130fe6384aabf7093e4c48042caf782ec216f536161)]);
        vk.delta = Pairing.G2Point([uint256(0x2ff7d3502ae6c67e9d5e41ab06073e64cb6b4d91047e4c2c712f427e122e90e6), uint256(0x1045cf086a79e0c4f340fbce4e5084cacc3a97e4c5f540236b81ff5ac87f2be8)], [uint256(0x21079b97123da57d759e85292733a532f8cbe19077ab3f43b43562f1ebab03f3), uint256(0x051808fc0f6314abea53a9b90ed9b53db225c30361b049fa42c051298f7cbd4e)]);
        vk.gamma_abc = new Pairing.G1Point[](9);
        vk.gamma_abc[0] = Pairing.G1Point(uint256(0x3057c1bd68ebb4d4be94a0e99bfa5cfcac79328035e2db2db245129dfcc5fa1e), uint256(0x27420bd1e1eccd5160316b0f9d410af224c149f8937fea26fc244238667b97cc));
        vk.gamma_abc[1] = Pairing.G1Point(uint256(0x2cb86d9588456d4bb599c3bd96469ca502c0a920f9cde1b59fba9ae3e935d4fe), uint256(0x0020e6012c60d79cdadf47d9f0f2d88060fca9d006023afac8e2664c124268f7));
        vk.gamma_abc[2] = Pairing.G1Point(uint256(0x2f4fefded924f8019b574d2415a1ce537b3376237b21466f563092b0499e0e8f), uint256(0x11056abc8d29e46ac7eeba8e544414179b314b68df65aeb04f3b133aa6da1147));
        vk.gamma_abc[3] = Pairing.G1Point(uint256(0x1deaf2ee585f1096fff7fe793118409eb577f385ec046f925332ba747c86d335), uint256(0x2697cfef879f0c9d0f6cfb6e8be2129770c2bd8320da5d3726a5a8edef6a1530));
        vk.gamma_abc[4] = Pairing.G1Point(uint256(0x149cc5b6fa65f213fad57d1cfc301dd1e258713e7d75ac8d6b6a2429d63c653e), uint256(0x119e6c9a211b9369edad21c2aebad5eb624a47e7092f88377719e6d6601fe905));
        vk.gamma_abc[5] = Pairing.G1Point(uint256(0x2f4b8adb46c86a0e8abd6d7935d1670deebbd2e2e83b23068f03710a0e6745bc), uint256(0x21cc24431d8ae296c48dba75a68fbeabe0daaf4ae40df392581009104e98821b));
        vk.gamma_abc[6] = Pairing.G1Point(uint256(0x1cf63714c2cd7c0a1cfba056a6c21633e9f8ecbb5fa8ea642065f49e48e94820), uint256(0x0f4730a6d2ba4c34f08a52abc151502084b87dad927d4e6b5d91d099612cf447));
        vk.gamma_abc[7] = Pairing.G1Point(uint256(0x1ffe2e8c8d27b51b38296b77593703e2fea62244466c5393b35209ff73bbc7b2), uint256(0x031d0e769033edb580718f173ff835671ea40cc33966b5ad037939f614341a48));
        vk.gamma_abc[8] = Pairing.G1Point(uint256(0x2b654bb3c9e445ea20c5047650d661cd34a7446ef2ddad52fbd57b59a8f78110), uint256(0x1f0b4a9060a60f0949cd45d2806856fefa0eb9bed6d731eab9009494db7e2905));
    }
    function verify(uint[] memory input, Proof memory proof) internal view returns (uint) {
        uint256 snark_scalar_field = 21888242871839275222246405745257275088548364400416034343698204186575808495617;
        VerifyingKey memory vk = verifyingKey();
        require(input.length + 1 == vk.gamma_abc.length);
        // Compute the linear combination vk_x
        Pairing.G1Point memory vk_x = Pairing.G1Point(0, 0);
        for (uint i = 0; i < input.length; i++) {
            require(input[i] < snark_scalar_field);
            vk_x = Pairing.addition(vk_x, Pairing.scalar_mul(vk.gamma_abc[i + 1], input[i]));
        }
        vk_x = Pairing.addition(vk_x, vk.gamma_abc[0]);
        if(!Pairing.pairingProd4(
             proof.a, proof.b,
             Pairing.negate(vk_x), vk.gamma,
             Pairing.negate(proof.c), vk.delta,
             Pairing.negate(vk.alpha), vk.beta)) return 1;
        return 0;
    }
    function verifyTx(
            Proof memory proof, uint[8] memory input
        ) public view returns (bool r) {
        uint[] memory inputValues = new uint[](8);
        
        for(uint i = 0; i < input.length; i++){
            inputValues[i] = input[i];
        }
        if (verify(inputValues, proof) == 0) {
            return true;
        } else {
            return false;
        }
    }
}
