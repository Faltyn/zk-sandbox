//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

struct G1Point {
    uint X;
    uint Y;
}
struct G2Point {
    uint[2] X;
    uint[2] Y;
}
struct Proof {
    G1Point a;
    G2Point b;
    G1Point c;
}

interface IMintVerifier {
    function verifyTx(Proof memory proof, uint[8] memory input) external view returns (bool r);
}

interface IBurnVerifier {
    function verifyTx(Proof memory proof, uint[8] memory input) external view returns (bool r);
}

interface ITransferVerifier {
    function verifyTx(Proof memory proof, uint[16] memory input) external view returns (bool r);
}

contract TokenShield {
    IERC20 public token;
    IMintVerifier public mintVerifier;
    IBurnVerifier public burnVerifier;
    ITransferVerifier public transferVerifier;

    mapping(uint256 => bool) commitments;

    constructor(address _token, address _mintVerifier, address _transferVerifier, address _burnVerifier) {
        require(_token != address(0), "Token address bad.");
        require(_mintVerifier != address(0), "Mint verifier address bad.");
        require(_burnVerifier != address(0), "Burn verifier address bad.");
        require(_transferVerifier != address(0), "Transfer verifier address bad.");
        token = IERC20(_token);
        mintVerifier = IMintVerifier(_mintVerifier);
        burnVerifier = IBurnVerifier(_burnVerifier);
        transferVerifier = ITransferVerifier(_transferVerifier);
    }

    function mint(uint256 _amount, uint256[8] memory _commitment, uint256[8] memory _proof) external {
        Proof memory userProof;
        userProof.a = G1Point({X: _proof[0], Y:_proof[1]});
        userProof.b = G2Point({X: [_proof[2], _proof[3]], Y:[_proof[4], _proof[5]]});
        userProof.c = G1Point({X: _proof[6], Y:_proof[7]});

        bool result = mintVerifier.verifyTx(userProof, _commitment);
        require(result == true, "Proof verification failed");

        uint256 commitment = 0;
        commitment |= _commitment[0];
        commitment |= _commitment[1] << 32;
        commitment |= _commitment[2] << 64;
        commitment |= _commitment[3] << 96;
        commitment |= _commitment[4] << 128;
        commitment |= _commitment[5] << 160;
        commitment |= _commitment[6] << 192;
        commitment |= _commitment[7] << 224;

        commitments[commitment] = true;
        token.transferFrom(msg.sender, address(this), _amount);
    }

    function transfer(uint256[8] memory _sCommitment, uint256[8] memory _dCommitment, uint256[8] memory _proof) external {
        uint256 sCommitment = 0;
        sCommitment |= _sCommitment[0];
        sCommitment |= _sCommitment[1] << 32;
        sCommitment |= _sCommitment[2] << 64;
        sCommitment |= _sCommitment[3] << 96;
        sCommitment |= _sCommitment[4] << 128;
        sCommitment |= _sCommitment[5] << 160;
        sCommitment |= _sCommitment[6] << 192;
        sCommitment |= _sCommitment[7] << 224;

        uint256 dCommitment = 0;
        dCommitment |= _dCommitment[0];
        dCommitment |= _dCommitment[1] << 32;
        dCommitment |= _dCommitment[2] << 64;
        dCommitment |= _dCommitment[3] << 96;
        dCommitment |= _dCommitment[4] << 128;
        dCommitment |= _dCommitment[5] << 160;
        dCommitment |= _dCommitment[6] << 192;
        dCommitment |= _dCommitment[7] << 224;

        require(commitments[sCommitment] == true, "Unknown source commitment");
        require(commitments[dCommitment] == false, "existing destination commitment");

        Proof memory userProof;
        userProof.a = G1Point({X: _proof[0], Y:_proof[1]});
        userProof.b = G2Point({X: [_proof[2], _proof[3]], Y:[_proof[4], _proof[5]]});
        userProof.c = G1Point({X: _proof[6], Y:_proof[7]});

        uint256[16] memory input;
        uint inputI = 0;
        for (uint i = 0; i < _sCommitment.length; i++) {
            input[inputI] = _sCommitment[i];
            inputI++;
        }
        for (uint i = 0; i < _dCommitment.length; i++) {
            input[inputI] = _dCommitment[i];
            inputI++;
        }

        bool result = transferVerifier.verifyTx(userProof, input);
        require(result == true, "Proof verification failed");

        delete commitments[sCommitment];
        commitments[dCommitment] = true;
    }

    function burn(uint256 amount, uint256[8] memory _commitment, uint256[8] memory _proof) external {
        uint256 commitment = 0;
        commitment |= _commitment[0];
        commitment |= _commitment[1] << 32;
        commitment |= _commitment[2] << 64;
        commitment |= _commitment[3] << 96;
        commitment |= _commitment[4] << 128;
        commitment |= _commitment[5] << 160;
        commitment |= _commitment[6] << 192;
        commitment |= _commitment[7] << 224;

        require(commitments[commitment] == true, "Unknown commitment");

        Proof memory userProof;
        userProof.a = G1Point({X: _proof[0], Y:_proof[1]});
        userProof.b = G2Point({X: [_proof[2], _proof[3]], Y:[_proof[4], _proof[5]]});
        userProof.c = G1Point({X: _proof[6], Y:_proof[7]});
        
        bool result = burnVerifier.verifyTx(userProof, _commitment);
        require(result == true, "Proof verification failed");

        delete commitments[commitment];
        token.transfer(msg.sender, amount);
    }
}