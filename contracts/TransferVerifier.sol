// This file is MIT Licensed.
//
// Copyright 2017 Christian Reitwiessner
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
pragma solidity ^0.8.0;
library Pairing {
    struct G1Point {
        uint X;
        uint Y;
    }
    // Encoding of field elements is: X[0] * z + X[1]
    struct G2Point {
        uint[2] X;
        uint[2] Y;
    }
    /// @return the generator of G1
    function P1() pure internal returns (G1Point memory) {
        return G1Point(1, 2);
    }
    /// @return the generator of G2
    function P2() pure internal returns (G2Point memory) {
        return G2Point(
            [10857046999023057135944570762232829481370756359578518086990519993285655852781,
             11559732032986387107991004021392285783925812861821192530917403151452391805634],
            [8495653923123431417604973247489272438418190587263600148770280649306958101930,
             4082367875863433681332203403145435568316851327593401208105741076214120093531]
        );
    }
    /// @return the negation of p, i.e. p.addition(p.negate()) should be zero.
    function negate(G1Point memory p) pure internal returns (G1Point memory) {
        // The prime q in the base field F_q for G1
        uint q = 21888242871839275222246405745257275088696311157297823662689037894645226208583;
        if (p.X == 0 && p.Y == 0)
            return G1Point(0, 0);
        return G1Point(p.X, q - (p.Y % q));
    }
    /// @return r the sum of two points of G1
    function addition(G1Point memory p1, G1Point memory p2) internal view returns (G1Point memory r) {
        uint[4] memory input;
        input[0] = p1.X;
        input[1] = p1.Y;
        input[2] = p2.X;
        input[3] = p2.Y;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 6, input, 0xc0, r, 0x60)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require(success);
    }


    /// @return r the product of a point on G1 and a scalar, i.e.
    /// p == p.scalar_mul(1) and p.addition(p) == p.scalar_mul(2) for all points p.
    function scalar_mul(G1Point memory p, uint s) internal view returns (G1Point memory r) {
        uint[3] memory input;
        input[0] = p.X;
        input[1] = p.Y;
        input[2] = s;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 7, input, 0x80, r, 0x60)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require (success);
    }
    /// @return the result of computing the pairing check
    /// e(p1[0], p2[0]) *  .... * e(p1[n], p2[n]) == 1
    /// For example pairing([P1(), P1().negate()], [P2(), P2()]) should
    /// return true.
    function pairing(G1Point[] memory p1, G2Point[] memory p2) internal view returns (bool) {
        require(p1.length == p2.length);
        uint elements = p1.length;
        uint inputSize = elements * 6;
        uint[] memory input = new uint[](inputSize);
        for (uint i = 0; i < elements; i++)
        {
            input[i * 6 + 0] = p1[i].X;
            input[i * 6 + 1] = p1[i].Y;
            input[i * 6 + 2] = p2[i].X[1];
            input[i * 6 + 3] = p2[i].X[0];
            input[i * 6 + 4] = p2[i].Y[1];
            input[i * 6 + 5] = p2[i].Y[0];
        }
        uint[1] memory out;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 8, add(input, 0x20), mul(inputSize, 0x20), out, 0x20)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require(success);
        return out[0] != 0;
    }
    /// Convenience method for a pairing check for two pairs.
    function pairingProd2(G1Point memory a1, G2Point memory a2, G1Point memory b1, G2Point memory b2) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](2);
        G2Point[] memory p2 = new G2Point[](2);
        p1[0] = a1;
        p1[1] = b1;
        p2[0] = a2;
        p2[1] = b2;
        return pairing(p1, p2);
    }
    /// Convenience method for a pairing check for three pairs.
    function pairingProd3(
            G1Point memory a1, G2Point memory a2,
            G1Point memory b1, G2Point memory b2,
            G1Point memory c1, G2Point memory c2
    ) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](3);
        G2Point[] memory p2 = new G2Point[](3);
        p1[0] = a1;
        p1[1] = b1;
        p1[2] = c1;
        p2[0] = a2;
        p2[1] = b2;
        p2[2] = c2;
        return pairing(p1, p2);
    }
    /// Convenience method for a pairing check for four pairs.
    function pairingProd4(
            G1Point memory a1, G2Point memory a2,
            G1Point memory b1, G2Point memory b2,
            G1Point memory c1, G2Point memory c2,
            G1Point memory d1, G2Point memory d2
    ) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](4);
        G2Point[] memory p2 = new G2Point[](4);
        p1[0] = a1;
        p1[1] = b1;
        p1[2] = c1;
        p1[3] = d1;
        p2[0] = a2;
        p2[1] = b2;
        p2[2] = c2;
        p2[3] = d2;
        return pairing(p1, p2);
    }
}

contract TransferVerifier {
    using Pairing for *;
    struct VerifyingKey {
        Pairing.G1Point alpha;
        Pairing.G2Point beta;
        Pairing.G2Point gamma;
        Pairing.G2Point delta;
        Pairing.G1Point[] gamma_abc;
    }
    struct Proof {
        Pairing.G1Point a;
        Pairing.G2Point b;
        Pairing.G1Point c;
    }
    function verifyingKey() pure internal returns (VerifyingKey memory vk) {
        vk.alpha = Pairing.G1Point(uint256(0x2265f39fd866de018ce1a7d19d8a944ad715ef58c159be41160e17172c8b523f), uint256(0x274d0165b3774f9c0deb6cf467b26512b1d6c76a76cb9302bfaf57e2bc25c353));
        vk.beta = Pairing.G2Point([uint256(0x2c8c751b7328218f2fb8e945a76ba38dd4e36003a8f7624d7e0a6e000c77c0b7), uint256(0x28d058870857a7d642a7f367f75fe7129ecdbd569e816c116993291ee8007efc)], [uint256(0x088aa05397a2883b8c59c0cf87f62ca26bd49f95baf0fe06ee34ff360c05028b), uint256(0x1b84bdc41edf511334886f499c54ac516fb0b96e6f8c481a98888ca5e40db1c9)]);
        vk.gamma = Pairing.G2Point([uint256(0x117153f4210dac5a8476139b6cf5bf4a190d27f08505153abf2d99b5200419ef), uint256(0x12fb2ad50e2b1be75182c3b20ad4589e807d0e81f977e8da868c6ed2d1b48804)], [uint256(0x1e7c23f9ba43e1e17e7b620c1984e1114811ed9f5eb6c5c2a9491fdb7470f133), uint256(0x11f6e3379c4e12e7c35f154d7a33a9310b5eadc148ac3a1e85b4042d6a49a606)]);
        vk.delta = Pairing.G2Point([uint256(0x2b81c98fab0a9662e7ef5f94c40d3a05f151f99a6735a1c8cf0d103bd16a0e86), uint256(0x0fb4ebefb6fb52d7620f11f7f746063c5bcd115b90c2127011af7f8a7ec4d7ee)], [uint256(0x162d5f33c2dfd81026802442e40b1075df0b1d2a9c57a5ccf211b4cd26a84a01), uint256(0x2489b4889aaf9979ef838475e6bdff2ca6226b104a26dfdc6e571a72b96a347c)]);
        vk.gamma_abc = new Pairing.G1Point[](17);
        vk.gamma_abc[0] = Pairing.G1Point(uint256(0x26c2b52e61bcc2e064ae3651f8f4efe0cb148f2a2aca2c346b8d6979c8a6127a), uint256(0x00f3e66ad3796c02ee24b10c502486e67ea24bda875638be5b35e4f6835a52a6));
        vk.gamma_abc[1] = Pairing.G1Point(uint256(0x15f4d7f2a86da7806ac8ea453ff4b3d22f61628a4c2546989994e166efbb1d9d), uint256(0x17d0ea510ee60e35533ffbc6253f17bb1094cd32c5d4bd959b0029548e5fd431));
        vk.gamma_abc[2] = Pairing.G1Point(uint256(0x0963c8373d1757a7973c979f03b5c9e9b5c0289130bf0f5043f66f28eedc0fa7), uint256(0x1e97caa23fe65913827383f8a3d19a1526821693f5bc53e7a73d0142b8d9d400));
        vk.gamma_abc[3] = Pairing.G1Point(uint256(0x12f4e4c6a4f2a802a0348c276298fa6253e9dc2479606fd7856dce3faa15fcf6), uint256(0x14058f17e24a58d2729773552ca1e4d3bb54c73ae218cfd1872e96e1a4785fba));
        vk.gamma_abc[4] = Pairing.G1Point(uint256(0x22e9dc48560206f6e410d21b4390352faedf3ce1fb654298ad10d2ad6d3743a9), uint256(0x13a17a6de61a90350ff88dc802b80099d0ed82359234a53cb0845b7b1cbcf3e8));
        vk.gamma_abc[5] = Pairing.G1Point(uint256(0x288b6847d1375a52a3e1002a2d771ac6fb8938d82033f5ad3828d61e24799996), uint256(0x22b6819fb13382d2d798e1f04810b256cbbf71840273d631e09567041c11877d));
        vk.gamma_abc[6] = Pairing.G1Point(uint256(0x1451b77de38dd8473dacb6a0f87586f236ca5392e0e19f88fff0ccd17e335c67), uint256(0x1339a86b4f754b967b4f73f52bfdf99713211eb6d61bd9712047071a6cb27d85));
        vk.gamma_abc[7] = Pairing.G1Point(uint256(0x03860b089f3c335fbb725e96b53b4a56b2d33b109e86f5b23260047658052e64), uint256(0x247e57fee412397178607d9596ac89c2031165947101d31c378496287651266c));
        vk.gamma_abc[8] = Pairing.G1Point(uint256(0x2992d1c5b9a3d9473c01dda3584e4b8786f8b8411b62e5d1aa61b137b55cded8), uint256(0x0ea164226b8152d22b10fdb0720c74750062cb9ad6991d3561df2bb8c6d41458));
        vk.gamma_abc[9] = Pairing.G1Point(uint256(0x105385483526f760ac0e88f63f666ccd90b2575da873456553b459cd20aafdab), uint256(0x1d87dd6f4908f4b49d1b620e03d95b36e84949b532754a059e56687d0f901ebc));
        vk.gamma_abc[10] = Pairing.G1Point(uint256(0x2a97eb830f86d7b91697b7115a5f097d78b2e4e6cf443d02079564ce57622449), uint256(0x200abd6c62321a68fb9cc4e48783b2fa136513f88a37cb8042bc643154b58615));
        vk.gamma_abc[11] = Pairing.G1Point(uint256(0x2184e21625e5b47f5303d0973915dea590feccfc3717d05573bcde8bf238bf52), uint256(0x0991bfabb44eebf9a909982dfed830deed1544568c85388d3196db6756c72afb));
        vk.gamma_abc[12] = Pairing.G1Point(uint256(0x0b256c37425cbdeededc61123ffa4e1072fe181e35832d47be23b44d15f697bc), uint256(0x197a84cdc942853a1d2445dd83032036a64801ce3488130ff06cfcba3c0cca5f));
        vk.gamma_abc[13] = Pairing.G1Point(uint256(0x0531b532675659198bfb562c4c757db1fa428cd09c545bfa3555bb055bbf9b05), uint256(0x1cd7cce2d7371a2569d7922c566d91ca39d7f7b43238a95b98ebf6d4d6c54f9b));
        vk.gamma_abc[14] = Pairing.G1Point(uint256(0x16c81d9b363e6b45bc119beb795108100954437499319cdf0a2915496472b798), uint256(0x1c3cd336700072394cab124f8f10e978676837d7edbd20b5fcccd31bbc967df1));
        vk.gamma_abc[15] = Pairing.G1Point(uint256(0x159fc6634ac8b33f86fb3796f65fc9b613c3d622b820fdf662a16fa33c98cc7c), uint256(0x20a6a03102244800f96a6167cb93121178c3ad79ec3013d1deb940eac591d2a2));
        vk.gamma_abc[16] = Pairing.G1Point(uint256(0x11a697a4b1d64dd73b7225d21d367343164a415bfdfa935299f9a98a56c108f9), uint256(0x244cd60c96922f66b3c1c28ef82b62543d35f68b540529b71b4d052c03ce95b2));
    }
    function verify(uint[] memory input, Proof memory proof) internal view returns (uint) {
        uint256 snark_scalar_field = 21888242871839275222246405745257275088548364400416034343698204186575808495617;
        VerifyingKey memory vk = verifyingKey();
        require(input.length + 1 == vk.gamma_abc.length);
        // Compute the linear combination vk_x
        Pairing.G1Point memory vk_x = Pairing.G1Point(0, 0);
        for (uint i = 0; i < input.length; i++) {
            require(input[i] < snark_scalar_field);
            vk_x = Pairing.addition(vk_x, Pairing.scalar_mul(vk.gamma_abc[i + 1], input[i]));
        }
        vk_x = Pairing.addition(vk_x, vk.gamma_abc[0]);
        if(!Pairing.pairingProd4(
             proof.a, proof.b,
             Pairing.negate(vk_x), vk.gamma,
             Pairing.negate(proof.c), vk.delta,
             Pairing.negate(vk.alpha), vk.beta)) return 1;
        return 0;
    }
    function verifyTx(
            Proof memory proof, uint[16] memory input
        ) public view returns (bool r) {
        uint[] memory inputValues = new uint[](16);
        
        for(uint i = 0; i < input.length; i++){
            inputValues[i] = input[i];
        }
        if (verify(inputValues, proof) == 0) {
            return true;
        } else {
            return false;
        }
    }
}
