//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TestToken is ERC20 {
    constructor(address _owner1, address _owner2) ERC20("TestToken", "TST") {
        _mint(_owner1, 1000000000000);
        _mint(_owner2, 1000000000000);
    }
}