// This file is MIT Licensed.
//
// Copyright 2017 Christian Reitwiessner
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
pragma solidity ^0.8.0;
library Pairing {
    struct G1Point {
        uint X;
        uint Y;
    }
    // Encoding of field elements is: X[0] * z + X[1]
    struct G2Point {
        uint[2] X;
        uint[2] Y;
    }
    /// @return the generator of G1
    function P1() pure internal returns (G1Point memory) {
        return G1Point(1, 2);
    }
    /// @return the generator of G2
    function P2() pure internal returns (G2Point memory) {
        return G2Point(
            [10857046999023057135944570762232829481370756359578518086990519993285655852781,
             11559732032986387107991004021392285783925812861821192530917403151452391805634],
            [8495653923123431417604973247489272438418190587263600148770280649306958101930,
             4082367875863433681332203403145435568316851327593401208105741076214120093531]
        );
    }
    /// @return the negation of p, i.e. p.addition(p.negate()) should be zero.
    function negate(G1Point memory p) pure internal returns (G1Point memory) {
        // The prime q in the base field F_q for G1
        uint q = 21888242871839275222246405745257275088696311157297823662689037894645226208583;
        if (p.X == 0 && p.Y == 0)
            return G1Point(0, 0);
        return G1Point(p.X, q - (p.Y % q));
    }
    /// @return r the sum of two points of G1
    function addition(G1Point memory p1, G1Point memory p2) internal view returns (G1Point memory r) {
        uint[4] memory input;
        input[0] = p1.X;
        input[1] = p1.Y;
        input[2] = p2.X;
        input[3] = p2.Y;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 6, input, 0xc0, r, 0x60)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require(success);
    }


    /// @return r the product of a point on G1 and a scalar, i.e.
    /// p == p.scalar_mul(1) and p.addition(p) == p.scalar_mul(2) for all points p.
    function scalar_mul(G1Point memory p, uint s) internal view returns (G1Point memory r) {
        uint[3] memory input;
        input[0] = p.X;
        input[1] = p.Y;
        input[2] = s;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 7, input, 0x80, r, 0x60)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require (success);
    }
    /// @return the result of computing the pairing check
    /// e(p1[0], p2[0]) *  .... * e(p1[n], p2[n]) == 1
    /// For example pairing([P1(), P1().negate()], [P2(), P2()]) should
    /// return true.
    function pairing(G1Point[] memory p1, G2Point[] memory p2) internal view returns (bool) {
        require(p1.length == p2.length);
        uint elements = p1.length;
        uint inputSize = elements * 6;
        uint[] memory input = new uint[](inputSize);
        for (uint i = 0; i < elements; i++)
        {
            input[i * 6 + 0] = p1[i].X;
            input[i * 6 + 1] = p1[i].Y;
            input[i * 6 + 2] = p2[i].X[1];
            input[i * 6 + 3] = p2[i].X[0];
            input[i * 6 + 4] = p2[i].Y[1];
            input[i * 6 + 5] = p2[i].Y[0];
        }
        uint[1] memory out;
        bool success;
        assembly {
            success := staticcall(sub(gas(), 2000), 8, add(input, 0x20), mul(inputSize, 0x20), out, 0x20)
            // Use "invalid" to make gas estimation work
            switch success case 0 { invalid() }
        }
        require(success);
        return out[0] != 0;
    }
    /// Convenience method for a pairing check for two pairs.
    function pairingProd2(G1Point memory a1, G2Point memory a2, G1Point memory b1, G2Point memory b2) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](2);
        G2Point[] memory p2 = new G2Point[](2);
        p1[0] = a1;
        p1[1] = b1;
        p2[0] = a2;
        p2[1] = b2;
        return pairing(p1, p2);
    }
    /// Convenience method for a pairing check for three pairs.
    function pairingProd3(
            G1Point memory a1, G2Point memory a2,
            G1Point memory b1, G2Point memory b2,
            G1Point memory c1, G2Point memory c2
    ) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](3);
        G2Point[] memory p2 = new G2Point[](3);
        p1[0] = a1;
        p1[1] = b1;
        p1[2] = c1;
        p2[0] = a2;
        p2[1] = b2;
        p2[2] = c2;
        return pairing(p1, p2);
    }
    /// Convenience method for a pairing check for four pairs.
    function pairingProd4(
            G1Point memory a1, G2Point memory a2,
            G1Point memory b1, G2Point memory b2,
            G1Point memory c1, G2Point memory c2,
            G1Point memory d1, G2Point memory d2
    ) internal view returns (bool) {
        G1Point[] memory p1 = new G1Point[](4);
        G2Point[] memory p2 = new G2Point[](4);
        p1[0] = a1;
        p1[1] = b1;
        p1[2] = c1;
        p1[3] = d1;
        p2[0] = a2;
        p2[1] = b2;
        p2[2] = c2;
        p2[3] = d2;
        return pairing(p1, p2);
    }
}

contract MintVerifier {
    using Pairing for *;
    struct VerifyingKey {
        Pairing.G1Point alpha;
        Pairing.G2Point beta;
        Pairing.G2Point gamma;
        Pairing.G2Point delta;
        Pairing.G1Point[] gamma_abc;
    }
    struct Proof {
        Pairing.G1Point a;
        Pairing.G2Point b;
        Pairing.G1Point c;
    }
    function verifyingKey() pure internal returns (VerifyingKey memory vk) {
        vk.alpha = Pairing.G1Point(uint256(0x106f36df68d1caaeb101e8575e8e47a5c99eeffa6663f911d4ad87da0a3bf31b), uint256(0x24431d4dc359c08a35b6c78fbeab86b41bcc1a8c9d07fee6045eeb193ced26b2));
        vk.beta = Pairing.G2Point([uint256(0x0a8f7a0fd6d9b8e6e6bbd359a496ed9cb77769b9582069512fb945b2028a51ce), uint256(0x06df97a4b78bb179c74de6ebecb1614f43c1d5bccc8fdbe81d6c18bddc4b8032)], [uint256(0x02e49f9d23410fd44f9bbf7532c1ffd44bbffe620bbb52b290665d03728d4288), uint256(0x0b72a954ed507cb18a6b86066fccd8c96a101fbe845ffe987dad5484d49197a4)]);
        vk.gamma = Pairing.G2Point([uint256(0x05f61f2610d2606cec6869c2c421b1ff539b52095006026af65330f7b94e5ea3), uint256(0x1620b90f60652383e5ae0c3883db52d0a1d855f4f922fee10dc996516fc47c83)], [uint256(0x0770015d70b0d16419d87a88d1748d4760f0631bcd227ce8a1a69c0b8d5f46c0), uint256(0x270bcd5b47fa502b90ceb44007ac60abb07ec7705348fcd28164a1d861832b09)]);
        vk.delta = Pairing.G2Point([uint256(0x2204c92537f035a6bdb652ae58b80b7909e6b199fc3ba79a74d385922673fd4b), uint256(0x20694ca36e902057d81014ba03dba2445c2510a6d4e27bcf10111fdaf6064ada)], [uint256(0x0c071eee08d9e77c4dc33caf829e92af632174176e1a9b3821da7de9a0140a1d), uint256(0x2315a90dca25308cb795eca4906178a789fcbd679174984e3880c2dbecb1eb71)]);
        vk.gamma_abc = new Pairing.G1Point[](9);
        vk.gamma_abc[0] = Pairing.G1Point(uint256(0x14ba1c39ebe8833e88c6a70447437bc2d04017140ef561b3536aa8e295e37c19), uint256(0x17e9aa8829a15eb63c73c33e4cc77c0f04c5e72f8db4700397d72492a531a422));
        vk.gamma_abc[1] = Pairing.G1Point(uint256(0x1db99d260d35859656eb5328d8ae1bc19c09724d214b3e8139a5a7c44fd3b48a), uint256(0x1e67788e93042d1cdf968cf55b5a638cb9cbb958e93ae2bd30d4574a9c4db30b));
        vk.gamma_abc[2] = Pairing.G1Point(uint256(0x03781900121bc7a78f70610e2f2c63c53e8aa6045411545abbbfc631925af73d), uint256(0x123cbc1fce3aa03a87c2d78bd9f6b689a2f9850cda7ad3bf2441309fe032431d));
        vk.gamma_abc[3] = Pairing.G1Point(uint256(0x10f5b82d882a9c4b61986126dc008f886c6e9c4f7c3bfe462bc7d0b0a017ea0f), uint256(0x0a8d2138692c78fa36515644fbfe6423601342f906c84398d808e17af47bbb96));
        vk.gamma_abc[4] = Pairing.G1Point(uint256(0x278cc1368b25b0013276b45ee2da194407d0a058a4a12658ef83670d26d5e6b8), uint256(0x04d03ece13a08c6b03a3566bb7019a76abbb7dd6a03e89dafd37c22721d93224));
        vk.gamma_abc[5] = Pairing.G1Point(uint256(0x0be942b664e1a2369a755d13bc8c75575f7663b8cdc26259304cb6ca0ce0029b), uint256(0x27e83131bee7d8e8b0186fd624c63f497da66ef8bd6e7054e0f7a8b31f335cce));
        vk.gamma_abc[6] = Pairing.G1Point(uint256(0x1b6cb32ae916013623156879bd91c96b93fd6e3c14ee81b939fd00c5c6a124df), uint256(0x09b4c5cd8bba19bbc5c30c627e4750d81fbdba44f56b435d22272feb0b7c0142));
        vk.gamma_abc[7] = Pairing.G1Point(uint256(0x13c7c245d1adeac70d927f7bc2a37e9a43c0acbec989fe5c3d47f4fe27c7e045), uint256(0x1c5f21bac68e6dce8476ac4742c696cafc6b2c7a98017c5ecb27f45f8f94a997));
        vk.gamma_abc[8] = Pairing.G1Point(uint256(0x29119cbe7ddb0bf125703a900015c99356acb0be3ab6f5eeeefe336ff4b08e6f), uint256(0x04d04429744f1217fe423eb2146c7c242fd60389076d1fa6921826ab7b87565d));
    }
    function verify(uint[] memory input, Proof memory proof) internal view returns (uint) {
        uint256 snark_scalar_field = 21888242871839275222246405745257275088548364400416034343698204186575808495617;
        VerifyingKey memory vk = verifyingKey();
        require(input.length + 1 == vk.gamma_abc.length);
        // Compute the linear combination vk_x
        Pairing.G1Point memory vk_x = Pairing.G1Point(0, 0);
        for (uint i = 0; i < input.length; i++) {
            require(input[i] < snark_scalar_field);
            vk_x = Pairing.addition(vk_x, Pairing.scalar_mul(vk.gamma_abc[i + 1], input[i]));
        }
        vk_x = Pairing.addition(vk_x, vk.gamma_abc[0]);
        if(!Pairing.pairingProd4(
             proof.a, proof.b,
             Pairing.negate(vk_x), vk.gamma,
             Pairing.negate(proof.c), vk.delta,
             Pairing.negate(vk.alpha), vk.beta)) return 1;
        return 0;
    }
    function verifyTx(
            Proof memory proof, uint[8] memory input
        ) public view returns (bool r) {
        uint[] memory inputValues = new uint[](8);
        
        for(uint i = 0; i < input.length; i++){
            inputValues[i] = input[i];
        }
        if (verify(inputValues, proof) == 0) {
            return true;
        } else {
            return false;
        }
    }
}
