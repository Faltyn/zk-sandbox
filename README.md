Simple usecase/implementation of zk-snark proofs with the use of zokrates.

Please keep in mind that this was written just to learn how zksnarks can be used. 
In terms of quality/structure and functionality this is far from any production code.

Prequisites:
- installed zokrates 
- ganache/geth instance on 127.0.0.1:7545 by default (can be changed in truffle-config.js)

npm install 
npm run all

This will setup/build the circuts/proofs/keys, deploy contracts and run simple tests for mint transfer and burn.

